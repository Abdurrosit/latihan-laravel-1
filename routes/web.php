<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function() {
    return view('latihan.index');
});

route::get('/form', 'IndexController@register');
route::post('/welcome','AuthController@welcome');

Route::get('/table', function() {
    return view('table.data');
});
Route::get('/data-table', function() {
    return view('table.data-table');
});

//CRUD cast
Route::get('/cast/create', 'CastController@create');
//tambah data cast
Route::post('/cast', 'CastController@tambah');
//read
Route::get('/cast', 'CastController@index');
//tampil cast
Route::get('/cast/{cast_id}', 'CastController@tampil');
//edit cast
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//simpan data edit
Route::put('/cast/{cast_id}', 'CastController@update');
//delete cast
Route::delete('/cast/{cast_id}', 'CastController@hapus');