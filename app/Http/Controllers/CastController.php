<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function tambah(request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required'
        ]);
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
            ]);
            return redirect('/cast/create');
            
            
    }

    public function index(){
        $cast = DB::table('cast')->get();
        //dd($cast);
        return view('cast.index', compact('cast'));
    }

    public function tampil($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.tampil', compact('cast'));
        }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }
    public function update($id, request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required'
        ]);
        $edit = DB::table('cast')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'umur' => $request['umur'],
                  'bio' => $request['bio']
                ]);
        return redirect('/cast');
    }
    public function hapus($id){
        $cast = DB::table('game')->where('id', $id)->delete();
        return redirect('/game');
        }
}
