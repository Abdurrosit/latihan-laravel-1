@extends('layout.master')

@section('judul')
Edit Data cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">
@csrf
@method('PUT')
<div class="form-group">
  <label class="form-label">Nama</label>
  <input type="text" name="nama" class="form-control"  value="{{$cast->nama}}">
</div>
@error('nama')
<div class="alert alert-danger">
    {{ $message }}
</div>
@enderror
<div class="form-group">
  <label class="form-label">Umur</label>
  <input type="number" class="form-control" name="umur" value="{{$cast->umur}}">
</div>
@error('Umur')
<div class="alert alert-danger">
    {{ $message }}
</div>
@enderror
<div class="form-group">
  <label class="form-label">Biodata</label>
  <textarea class="form-control" name="bio" rows="3">{{$cast->bio}}</textarea>
</div>
@error('bio')
<div class="alert alert-danger">
    {{ $message }}
</div>
@enderror
<button type="submit" class="btn btn-primary mr-1">Edit Data</button>
<a href="/cast" class="btn btn-danger">Kembali</a>
</form>
@endsection