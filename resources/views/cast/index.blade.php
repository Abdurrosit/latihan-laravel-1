@extends('layout.master')

@section('judul')
Data Cast
@endsection

@push('script')
<script src="{{asset('Admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('Admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#cast").DataTable();
  });
</script>
@endpush
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
@endpush

@section('content')
<a href="/cast/create" class="btn btn-success mb-3">Tambah Data</a>
<table class="table table-hover table-bordered" id="cast">
  <thead class="bg bg-dark text-light">
    <tr>
      <th scope="col">NO</th>
      <th scope="col">NAMA CAST</th>
      <th scope="col">UMUR</th>
      <th scope="col">BIO</th>
      <th scope="col">AKSI</th>
    </tr>
  </thead>
  <tbody>
    @forelse($cast as $key=>$item)
    <tr>
        <td>{{$key+1}}</td>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>{{$item->bio}}</td>
        <td>
          <form action="/cast/{{$item->id}}" method="POST">
            <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
              @@csrf
              @@method('delete')
              <input type="submit" class="btn btn-sm-danger" value="delete">
            </form>
        </td>
    </tr>
    @empty
    <tr>
        <td>Data Kosong</td>
    </tr>
    @endforelse
  </tbody>
</table>
@endsection
