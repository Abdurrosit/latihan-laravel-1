@extends('layout.master')

@section('judul')
Tambah Data cast
@endsection

@section('content')
<form action="/cast" method="post">
@csrf
<div class="form-group">
  <label class="form-label">Nama</label>
  <input type="text" name="nama" class="form-control" >
</div>
@error('nama')
<div class="alert alert-danger">
    {{ $message }}
</div>
@enderror
<div class="form-group">
  <label class="form-label">Umur</label>
  <input type="number" class="form-control" name="umur" >
</div>
@error('Umur')
<div class="alert alert-danger">
    {{ $message }}
</div>
@enderror
<div class="form-group">
  <label class="form-label">Biodata</label>
  <textarea class="form-control" name="bio" rows="3"></textarea>
</div>
@error('bio')
<div class="alert alert-danger">
    {{ $message }}
</div>
@enderror
<button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection