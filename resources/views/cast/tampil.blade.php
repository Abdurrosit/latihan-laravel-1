@extends('layout.master')

@section('judul')
Detail cast {{$cast->nama}}
@endsection

@section('content')
<form action="">
    @csrf
    <div class="form-group">
        <label for="">Nama Cast</label><br>
        <input type="text" class="form-control" value="{{$cast->nama}}" readonly>
    </div>
    <div class="form-group">
        <label for="">Umur</label><br>
        <input type="text" class="form-control" value="{{$cast->umur}}" readonly>
    </div>
    <div class="form-group">
        <label for="">Biografi</label><br>
        <textarea class="form-control" cols="30" rows="5" readonly>{{$cast->bio}}</textarea>
    </div>
    <a href="/cast" class="btn btn-primary mt-3">Kembali</a>
</form>
@endsection